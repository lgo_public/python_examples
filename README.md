# Authentication example in python

Based on the documentation example : https://doc.exchange.lgo.markets/#_example

## Dependencies
- pycrypto (pip install pycrypto)
- websocket-client (pip install websocket-client)
- requests (pip install requests)


## Using the http live api

```python
import time
import json
import requests
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256

PRIVATE_KEY = """-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAojp7SlwPi4Ql5JPAbxP6+aG+z8HOGT9BsvSDHR/1gQ8xDXlM
crd7VtrhyQ31jR3rnc6aPx+lVxSn2YvQWyl/4pBQyV39hzJMoMLDz6TJjmPZOUGl
o2yAku9TntE70RaQcjf2bQA4ikZkMVesJ8TfGVsun7vaYTjJkXRkwdz4MXpXapqH
teWNVTjgteDZn3Z+MEA9W5OazmPVD/NsDzD9qBxDW0lwtvkHljjNQFit+dtN6zv8
th5aLcrcnXuSoXbe6RXRMSxCC0whTKfsveyBDCA+kSSPOgiBJyUuv863KSsvEd0k
kVB8Ox9U8z/V7/xpDLTNXkcfvIlUvC+3Hqi43wIDAQABAoIBADgttlpGzR9MUO75
946/xY7C41gAzkVR8YduQyVH1vWtdBgtZDrprS2juMKuMdV/ggNw81tesxwXzBR6
5VlcYqvru/4vrUcvNPgK2lJCx4WmsCeywxB314KKnFOIM4Wxoa3cEVsn02yW+cVY
jgZrl7KpL9ki7Xnzd2IGg4na4pwHK6EU+bD8+DV7IsMusum6veuEeKxJk2v6FG/Q
HZozl6aWQhOcYt2aLYue+Cx+qFnLV+zoCuB6lr2EmtqW5GDoSTk6ANJ3DJkX3Btk
jApRU5BOkipqWy2u7J8A6v9th0VQCXUA1/9ZD+Mq5PszK4pW4SW9RL2IVSRygLL5
nnslcXECgYEA0aJ9Itf5c0Te6py8H2eMFFAzj0b13PoXnQ3IgMIaTfVVf1i8Ksqy
VhWzFQNoy03WxCNyWduEqxTLXL2WLLCXnrsrArl8FE/PZnhH1U/RLxKl+O5xIN3z
90x70xLeQr/qBNRSOClhzToINNToHUQCgAEvDtZsNAERD5OJ99RdfBkCgYEAxhvZ
0WdT90s051oFW0rQSB9ZPD7M9oQ0NdJENy2gXzO97QlwT9xcC3ZjF6Oh2h3ZC62k
E32DKqV/gD6g6ANXW92z08Co+zUjFdKY1h7N1s/oxAc+vm+jfJnXKo72W4pehKkb
A6+PTvPLLOi0WDMG8c13ZALC662qxOZLd9V9e7cCgYEAsRGKmS/L5+04TPrue6g+
zbmgk1jguzITV/kYbomFJmwfN12AMrTbRZM2nH0wuuiYiztVj0i4GrmJvF/2xPC0
YMK/ZaG+iHmROYBHTIoKqrQZtNXSgGx5cV0NChBZ5A+uXz3n5MAvd+WYoOdk17Nm
WTCmyuWap6JvArUgSFD8VaECgYEAio1x8lVM0ThlAKTh+C5Dqx1ZoJvfZ02g4j9z
fA/KCKs8WqpuRTw9l7qtpRvJF64mXVeM2CDA+rOSj5O9n2au004j9aXZyQ8pwZpv
T9ltZp40Ed1rUW7srTk+1cH0pKMKZceLYDGJjdNNttPtRX4yjiyAIo8X2hK0y06x
W1cRktMCgYEAkIwoKjIDfj2EaKzCcxBSpxIrx2+6gB1qmC9v8PLRG4JDJTqj9gj+
HNI5gCSXj3NLzLFpnC/obG7G9jommp8uh0ojNzuyb0sc5y9SujeNdNbBKGkObprD
3LydCtIeSZ2KdhxBpWNEqa3PafBE+M/F0RnCGtm+cMm+oLw8VvktHhg=
-----END RSA PRIVATE KEY-----"""
ACCESS_KEY = '60715d6f-adc9-4792-9b4d-d705135a6173'

def sign(msg):
  private = RSA.importKey(PRIVATE_KEY)
  signer = PKCS1_v1_5.new(private)
  digest = SHA256.new()
  digest.update(msg.encode('utf-8'))
  return signer.sign(digest)

def create_header(url, body=None):
  timestamp = int(round(time.time() * 1000))
  prepared_url = url.replace('https://', '')
  to_sign = str(timestamp) + f"\n{prepared_url}" + (f"\n{body}" if body != None else "")
  signed = sign(to_sign).hex()
  authorization = f'LGO {ACCESS_KEY}:{signed}'
  return {'X-LGO-DATE': str(timestamp), 'Authorization': authorization}

def get(url):
  return requests.get(url, headers=create_header(url))

def post(url, data):
    return requests.post(url, data = data, headers=create_header(url, body=data))

balances = get('https://exchange-api.exchange.lgo.markets/v1/live/balances')

print(balances.text) 

order = {
  "type": "L",
  "side": "B",
  "product_id": "BTC-USD",
  "quantity": "2.5",
  "price": "9000.3",
  "reference": 34
}
 
order_id = post('https://exchange-api.exchange.lgo.markets/v1/live/orders', json.dumps(order))

print(order_id.text)
```

## Using a websocket

```python
import websocket
import time
try:
    import thread
except ImportError:
    import _thread as thread
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256


PRIVATE_KEY = """-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAojp7SlwPi4Ql5JPAbxP6+aG+z8HOGT9BsvSDHR/1gQ8xDXlM
crd7VtrhyQ31jR3rnc6aPx+lVxSn2YvQWyl/4pBQyV39hzJMoMLDz6TJjmPZOUGl
o2yAku9TntE70RaQcjf2bQA4ikZkMVesJ8TfGVsun7vaYTjJkXRkwdz4MXpXapqH
teWNVTjgteDZn3Z+MEA9W5OazmPVD/NsDzD9qBxDW0lwtvkHljjNQFit+dtN6zv8
th5aLcrcnXuSoXbe6RXRMSxCC0whTKfsveyBDCA+kSSPOgiBJyUuv863KSsvEd0k
kVB8Ox9U8z/V7/xpDLTNXkcfvIlUvC+3Hqi43wIDAQABAoIBADgttlpGzR9MUO75
946/xY7C41gAzkVR8YduQyVH1vWtdBgtZDrprS2juMKuMdV/ggNw81tesxwXzBR6
5VlcYqvru/4vrUcvNPgK2lJCx4WmsCeywxB314KKnFOIM4Wxoa3cEVsn02yW+cVY
jgZrl7KpL9ki7Xnzd2IGg4na4pwHK6EU+bD8+DV7IsMusum6veuEeKxJk2v6FG/Q
HZozl6aWQhOcYt2aLYue+Cx+qFnLV+zoCuB6lr2EmtqW5GDoSTk6ANJ3DJkX3Btk
jApRU5BOkipqWy2u7J8A6v9th0VQCXUA1/9ZD+Mq5PszK4pW4SW9RL2IVSRygLL5
nnslcXECgYEA0aJ9Itf5c0Te6py8H2eMFFAzj0b13PoXnQ3IgMIaTfVVf1i8Ksqy
VhWzFQNoy03WxCNyWduEqxTLXL2WLLCXnrsrArl8FE/PZnhH1U/RLxKl+O5xIN3z
90x70xLeQr/qBNRSOClhzToINNToHUQCgAEvDtZsNAERD5OJ99RdfBkCgYEAxhvZ
0WdT90s051oFW0rQSB9ZPD7M9oQ0NdJENy2gXzO97QlwT9xcC3ZjF6Oh2h3ZC62k
E32DKqV/gD6g6ANXW92z08Co+zUjFdKY1h7N1s/oxAc+vm+jfJnXKo72W4pehKkb
A6+PTvPLLOi0WDMG8c13ZALC662qxOZLd9V9e7cCgYEAsRGKmS/L5+04TPrue6g+
zbmgk1jguzITV/kYbomFJmwfN12AMrTbRZM2nH0wuuiYiztVj0i4GrmJvF/2xPC0
YMK/ZaG+iHmROYBHTIoKqrQZtNXSgGx5cV0NChBZ5A+uXz3n5MAvd+WYoOdk17Nm
WTCmyuWap6JvArUgSFD8VaECgYEAio1x8lVM0ThlAKTh+C5Dqx1ZoJvfZ02g4j9z
fA/KCKs8WqpuRTw9l7qtpRvJF64mXVeM2CDA+rOSj5O9n2au004j9aXZyQ8pwZpv
T9ltZp40Ed1rUW7srTk+1cH0pKMKZceLYDGJjdNNttPtRX4yjiyAIo8X2hK0y06x
W1cRktMCgYEAkIwoKjIDfj2EaKzCcxBSpxIrx2+6gB1qmC9v8PLRG4JDJTqj9gj+
HNI5gCSXj3NLzLFpnC/obG7G9jommp8uh0ojNzuyb0sc5y9SujeNdNbBKGkObprD
3LydCtIeSZ2KdhxBpWNEqa3PafBE+M/F0RnCGtm+cMm+oLw8VvktHhg=
-----END RSA PRIVATE KEY-----"""
ACCESS_KEY = '60715d6f-adc9-4792-9b4d-d705135a6173'
WS_URL = "wss://ws.sandbox.lgo.markets/"

def sign(msg):
    private = RSA.importKey(PRIVATE_KEY)
    signer = PKCS1_v1_5.new(private)
    digest = SHA256.new()
    digest.update(msg.encode('utf-8'))
    return signer.sign(digest)


def create_header(url):
  timestamp = int(round(time.time() * 1000))
  prepared_url = url.replace('wss://', '')
  to_sign = str(timestamp) + f"\n{prepared_url}"
  signed = sign(to_sign).hex()
  authorization = f'LGO {ACCESS_KEY}:{signed}'
  return {'X-LGO-DATE': str(timestamp), 'Authorization': authorization}

def on_message(ws, message):
    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
  ws.send('{"type": "subscribe","channels": [{"name": "balance"}]}')


if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp(WS_URL,
                                header=create_header(WS_URL),
                                on_message = on_message,
                                on_error = on_error,
                                on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

```
